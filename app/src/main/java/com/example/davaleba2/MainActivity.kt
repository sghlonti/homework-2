package com.example.davaleba2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()


    }

    private fun init(){
//        nameTextView.text = "Shako"
//        surnameTextView.text = "Ghlonti"
//        emailTextView.text = "Shakomail"
//        birthTextView.text ="1998"
//        sexTextView.text = "Male"
        openSecondActivity.setOnClickListener {
            openSecondActivity()
        }

    }

    private fun openSecondActivity(){
        val userModel = intent.getParcelableExtra("userModel") as? UserModel


        if (userModel != null) {
            nameTextView.text = userModel.name
        }
        if (userModel != null) {
            surnameTextView.text = userModel.surname
        }
        if (userModel != null) {
            emailTextView.text = userModel.email
        }
        if (userModel != null) {
            birthTextView.text = userModel.year.toString()
        }
        if (userModel != null) {
            sexTextView.text = userModel.sex
        }

        val intent = Intent(this,SecondActivity::class.java)

        startActivity(intent)
    }


}
