package com.example.davaleba2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init(){
        var bundle = intent.extras
        var user: UserModel? = bundle?.getParcelable("user")

        saveButton.setOnClickListener {
            if (user != null) {
                saveButton(user)
            }

        }
    }

     fun saveButton(s:UserModel){
         s.name = nameEditText.text.toString()
         s.surname = surnameEditText.text.toString()
         s.email = emailEditText.text.toString()
         s.year = birthEditText.text.toString().toInt()
         s.sex = sexEditText.text.toString()

         val intent2 = Intent(this,MainActivity::class.java)
         startActivity(intent2)
//         intent.putExtra("user",s)
//         setResult(Activity.RESULT_OK,intent)
//         finish()

    }

}






